public class RepasoGit {

	public static void main(String[] args) {
		// Declaración e inicialización de un array de enteros de tamaño 10
		int[] listaNum = new int[10];

// Declaramos variables de contador para saber cuántos números son pares y cuántos impares
int contadorPar=0, contadorImpar=0;

// Introducción de valores en el array y visualización de los mismos: en cada posición el contenido será su número de posición
		
		for(int i=0; i<listaNum.length; i++) {
			
			listaNum[i] = i;
			System.out.println(listaNum[i]);
		}

// Modificación del array y visualización del mismo: 
		
// en las posiciones pares del array se modificará el contenido que tenga por 0

También contamos cuántos números pares (cuyo valor sea 0) hay y cuantos impares (cuyo valor sea diferente a 0).
		
		for(int i=0; i<listaNum.length; i++) {
			
			if(listaNum[i]% 2 == 0) {
				listaNum[i] = 0;
				contadorPar++;
			}else {
				contadorImpar++;
			}

			
		}
// Visualización del número de ceros que hay en el array.
		
		System.out.println("Hay " + contadorPar + " números pares");
		
		
		// Visualización del número de elementos distintos de cero que hay en el array.
		
		System.out.println("Hay " + contadorImpar + " números impares");

	}

}
